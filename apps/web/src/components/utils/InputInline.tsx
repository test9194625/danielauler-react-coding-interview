import { useState } from "react";

export enum InputType {
    number="number",
    mail="mail",
    text="text"
}

export const InputInline = ({ text, type }: {text: string, type: InputType}) => {
    const [value, setValue] = useState(text);
    const [isEditing, setIsEditing] = useState(false);
    const [hasError, setHasError] = useState(false);

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        if (type === InputType.mail) {
            const mailRegex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;
            if (!mailRegex.test(e.target.value)) {
                setHasError(true);
                return;
            }
        } else {
            setHasError(false);
            setValue(e.target.value);
        }
    }

    if (isEditing) {
        return (
            <input
                type="text"
                value={value}
                onChange={handleChange}
                onBlur={() => setIsEditing(false)}
            />
        )
    }
    return (
        <div onClick={() => setIsEditing(true)}>
            {value}
        </div>
    )
}